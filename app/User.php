<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Route;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone_number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_role', 'user_id', 'role_id');
    }

    public function orders()
    {
        return $this->hasOne('App\Order', 'user_id');
    }

    public function blog()
    {
        return $this->hasMany(Blog::class);
    }

    public function editor()
    {
        return $this->hasMany(Editor::class);
    }

    public function check_route($route_name)
    {
        $route_id = Route::where('name', $route_name)->first()->id;
        $roles = $this->roles;
        $check = false;

        if ($roles) {
            foreach ($roles as $role) {
                foreach ($role->routes as $route) {
                    if ($route_id == $route->id) {
                        $check = true;
                    }
                }
            }
        }

        return $check;
    }
}
