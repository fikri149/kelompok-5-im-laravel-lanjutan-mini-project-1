<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Blog;

class BlogCreatedMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $blog;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Blog $blog)
    {
        $this->blog = $blog;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('fickreeyhidayat@gmail.com')
            ->view('blog_create_notifikasi')
            ->with([
                'judul' => $this->blog->judul,
                'name' => $this->blog->user->name,
            ]);
    }
}
