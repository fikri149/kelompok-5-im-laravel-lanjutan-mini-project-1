<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'tgl_jual', 'total', 'user_id'
    ];

    public function Product(){
        return $this->belongsToMany('App\Product',  'order_product', 'product_id', 'order_id');
    }

    public function User(){
        return $this->belongsTo('App\User');
    }
}
