<?php

namespace App\Listeners;

use App\Events\BlogCreatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\EditorBlogCreatedMail;
use Mail;

class SendEmailEditorBlogCreatedNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogCreatedEvent  $event
     * @return void
     */
    public function handle(BlogCreatedEvent $event)
    {
        Mail::to('editor@example.com')->send(new EditorBlogCreatedMail($event->blog));
    }
}
