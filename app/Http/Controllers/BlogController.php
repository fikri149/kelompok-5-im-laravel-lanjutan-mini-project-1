<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\BlogCreatedEvent;
use App\Blog;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => ['required', 'string'],
            'article' => ['required', 'string'],
        ]);

        $data = [];

        $blog = auth()->user()->blog()->create([
            'judul' => request('judul'),
            'article' => request('article'),
            'publish_status' => 0,
        ]);

        $data['blog'] = $blog;

        event(new BlogCreatedEvent($blog));

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Blog berhasil diinput',
            'data' => $data,
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
