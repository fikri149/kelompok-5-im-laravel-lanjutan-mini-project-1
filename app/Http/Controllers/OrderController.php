<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderProduct;
use App\User;
use Validator;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $orders = Order::all();
        foreach ($orders as $order)
        {
            $item_orders = OrderProduct::where('order_id', $order->id)->get();

            $data_orders = [
                'tgl_jual' => $order->tgl_jual,
                'total' => $order->total,
                'pelayan' => User::find($order->user_id)->first()->name,
                'item_order' => [ $item_orders ]
            ];

            $data[] = $data_orders;

        }



        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tgl_jual' => ['required', 'date'],
            'total' => ['required', 'string'],
        ]);
        $user = \Auth::user()->id;

        $order = new Order([
            'tgl_jual' => request('tgl_jual'),
            'total' => request('total'),
            'user_id'=> $user
        ]);

        if ($order->save())
        {

            $order_id = $order->id;
            
            for($a=0; $a< count($request->item_orders); $a++)
            {
                OrderProduct::create(array_merge($request->item_orders[$a], ['order_id' => $order_id]));
            }
            
        }

        return response('Data telah diinput');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        $item_order = OrderProduct::where('order_id', $order->id)->get();


        $data = [
            $order,
            'item_order' => $item_order
        ];

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        $order_product = OrderProduct::where('order_id', $order->id)->get();

        foreach ($order_product as $item)
        {
            OrderProduct::find($item->id)->delete();
        }

        $order->delete();

        return response('Data berhasil dihapus');

    }
}
