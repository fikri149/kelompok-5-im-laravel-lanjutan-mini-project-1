import Home from '../views/Home';
import About from '../views/About';

import CreateProduct from '../views/product/Create';
import ReadProduct from '../views/product/Read';
import ShowProduct from '../views/product/Show';

import CreateUser from '../views/user/Create';
import ReadUser from '../views/user/Read';
import ShowUser from '../views/user/Show';

export default {
    mode: 'history',

    linkActiveClass: 'active',

    routes: [{
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/about',
            name: 'pages.about',
            component: About
        },
        {
            path: '/user/create',
            name: 'user.create',
            component: CreateUser
        },
        {
            path: '/user',
            name: 'user',
            component: ReadUser
        },
        {
            path: '/user/:id',
            name: 'user.show',
            component: ShowUser
        },
        {
            path: '/product/create',
            name: 'product.create',
            component: CreateProduct
        },
        {
            path: '/product',
            name: 'product',
            component: ReadProduct
        },
        {
            path: '/product/:id',
            name: 'product.show',
            component: ShowProduct
        }
    ]
}
