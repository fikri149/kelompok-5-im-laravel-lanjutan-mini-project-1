<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Auth')->group(function () {
    Route::post('login', 'LoginController')->name('login');
    Route::post('logout', 'LogoutController')->name('logout');
});

Route::middleware(['auth:api', 'role'])->group(function () {

    // Auth untuk role Pemilik & Manajer

    Route::post('register', 'Auth\RegisterController')->name('register');

    //

    // Auth untuk role Pemilik, Manajer & Koki
    Route::post('product', 'ProductController@store')->name('create_product');
    Route::put('product/{product}', 'ProductController@update')->name('edit_product');
    Route::delete('product/{product}', 'ProductController@destroy')->name('delete_product');

    //

    // Auth untuk role Pemilik, Manajer, Koki & Pelayan

    Route::get('orders', 'OrderController@index')->name('all-orders');
    Route::get('orders/{orders}', 'OrderController@show')->name('show-orders');

    //

    // Auth untuk role Pemilik, Manajer, & Pelayan

    Route::post('orders', 'OrderController@store')->name('create-orders');
    Route::delete('orders/{orders}', 'OrderController@destroy')->name('delete_order');

    //
});

// Auth untuk guest

Route::get('products', 'ProductController@index')->name('all-products');
Route::get('products/{products}', 'ProductController@show')->name('show-products');


Route::post('products', 'ProductController@store')->name('create_products');
Route::put('products/{products}', 'ProductController@update')->name('edit_products');
Route::delete('products/{products}', 'ProductController@destroy')->name('delete_products');

Route::get('users', 'UserController@index')->name('all-users');
Route::get('users/{users}', 'UserController@show')->name('show-users');


Route::post('users', 'Auth\RegisterController')->name('create_users');
Route::put('users/{users}', 'UserController@update')->name('edit_users');
Route::delete('users/{users}', 'UserController@destroy')->name('delete_users');

// Route::middleware('auth:api')->group(function () {
//     Route::post('blog', 'BlogController@store')->name('create_blog');
// });
